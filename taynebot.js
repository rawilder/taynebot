const Discord = require('discord.js');
const path = require('path');

const Settings = require(path.join(__dirname, 'settings.json'));
let Tokens;
try {
    Tokens = require(path.join(__dirname, 'tokens.json'));
} catch (e) {
    Tokens = {};
}

/** Class representing TayneBot. */
class TayneBot {
    /**
     * constructor for TayneBot.
     * @constructor
     * @param {String} token - TayneBot's Discord Token.
     * @param {object} discordOpt - DiscordClient options.
     */
    constructor(token, discordOpt={}) {
        this.token = token;
        this.client = new Discord.Client(discordOpt || {autoReconnect: false});
        this.commands = {'interval': '', 'stopInterval': '', 'cancel': ''};
        this.usageList = '';
        this.settings = Settings;
        this.settings.tokens = Tokens;
        this.playingAudio = false;
        this.audioQueue = [];

        // store the RE as they're expensive to create
        this.cmd_re = new RegExp(
            `^${this.settings.bot_cmd}\\s*([^\\s]+)\\s*([^]*)\\s*`, 'i');

        this.isReady = false;
    }

    /**
     * TayneBot's onMessage function.
     * @return {function} on message function
     */
    onMessage() {
        return (message) => {
            // don't respond to own messages
            if (this.client.user.username === message.author.username) {
                return;
            }

            // console.log(this.usageList);
            // check if message is a command
            const cmdMatch = message.cleanContent.match(this.cmd_re);

            // not a known command
            if (!cmdMatch ||
                Object.keys(this.commands).indexOf(cmdMatch[1]) === -1) {
                if (message.content.match(new RegExp(
                        `^${this.settings.help_cmd}[\\s]*( .*)?$`, 'i'))) {
                    let helpText = 'I\'m Tayne, your latest dancer.' +
                        '\n\n```';
                    helpText += this.usageList;
                    helpText += '```';
                    message.channel.send(helpText);
                }
                return;
            }

            // process commands
            const cmd = cmdMatch[1];
            const cmdArgs = cmdMatch[2].trim();

            if (cmd === 'cancel') {
                message.guild.fetchMember(this.client.user).
                then((member) => {
                    if (member.voiceChannelID != null) {
                        member.voiceChannel.leave();
                    }
                });
                return;
            }

            if (cmd === 'bb' && message.author.id === '97908689599475712' ) {
                if (! this.playingAudio) {
                    message.guild.fetchMember(this.client.user).
                    then((member) => {
                        if (member.voiceChannelID == null) {
                            this.commands['meme'].
                                run(this, message.member.voiceChannel,
                                    'brownbricks.mp3');
                            message.delete();
                        }
                        // } else {
                        //     console.log('throwing away request');
                        // }
                    });
                }
                return;
            }

            if (cmd === 'interval') {
                if (this.interval == null) {
                    this.interval = setInterval((bot, message) => {
                        if (! this.playingAudio) {
                            message.guild.fetchMember(bot.client.user).
                            then((member) => {
                                if (member.voiceChannelID == null) {
                                    bot.commands['paula'].
                                        run(bot, message.member.voiceChannel,
                                            true);
                                }
                            });
                        }
                    }, 60000, this, message);
                }
                return;
            }

            if (cmd === 'stopInterval') {
                clearInterval(this.interval);
                this.interval = null;
                return;
            }

            // show usage if requested
            if (cmdArgs.match(new RegExp(
                '^[(-h)(h)(--help)(help)]'
            ))) {
                this.commands[cmd].showUsage(this, message);
                return;
            }

            let showUsage;

            try {
                if (this.commands[cmd].type === 'audio') {
                    if (! this.playingAudio) {
                        message.guild.fetchMember(this.client.user).
                        then((member) => {
                            if (member.voiceChannelID == null) {
                                showUsage = this.commands[cmd].
                                    run(this, message.member.voiceChannel,
                                        cmdArgs);
                            }
                            // } else {
                            //     console.log('throwing away request');
                            // }
                        });
                    }
                } else {
                    showUsage = this.commands[cmd].run(this, message, cmdArgs);
                }
            } catch (err) {
                message.channel.send(
                    'There was an error running the command:\n' +
                    '```\n' + err.toString() + '\n```');
                console.error(err);
                console.error(err.stack);
            }

            if (showUsage === true) {
                this.commands[cmd].showUsage(this, message);
            }
        };
    }

    /**
     * TayneBot's onReady function.
     * @return {function} on ready function
     */
    onReady() {
        return (() => {
            console.log('\nConnected to discord server!');
            console.log('Running initializations...');
            Object.keys(this.commands).filter((cmd) =>
                typeof this.commands[cmd].init === 'function')
            .forEach((cmd) => this.commands[cmd].init(this));
            this.isReady = true;
            console.log('Ready!');
        });
    }

    /**
     * TayneBot's serverNewMember function.
     * @return {function} on serverNewMember function
     */
    serverNewMember() {
        return ((server, user) =>
            this.client.send(user, this.usageList));
    }

    /**
     * TayneBot's onDisconnected function.
     * @return {function} on disconnected function
     */
    onDisconnected() {
        return () =>
        console.warn('Bot has been disconnected from server...');
    }

    /**
     * TayneBot's onError function.
     * @return {function} on error function
     */
    onError() {
        return ((err) => {
            console.error('error: ', err);
            console.error(err.trace);
        });
    }

    /**
     * TayneBot's onVoiceStateUpdate function.
     * @return {function} on voiceStateUpdate function
     */
    onVoiceStateUpdate() {
        return (oldMember, newMember) => {
            // don't respond to own messages
            if (this.client.user.username === oldMember.user.username) {
                return;
            }

            if (oldMember.voiceChannelID !=
                newMember.voiceChannelID) {
                if (! this.playingAudio) {
                    oldMember.guild.fetchMember(this.client.user).
                        then((member) => {
                            if (member.voiceChannelID == null) {
                                this.commands['paula'].
                                    run(this, newMember.voiceChannel, true);
                            }
                        });
                }
            }
        };
    }

    /**
     * TayneBot's onMessage commands.
     * @param {object} cmdList - list of commands to register
     */
    loadCommands(cmdList) {
        this.usageList = '';
        cmdList.forEach((cmd) => {
            const fullpath = path.join(__dirname, 'commands', cmd, `${cmd}.js`);
            const script = require(fullpath);
            this.commands[cmd] = script;

            const usageObj = script.usage;
            if (usageObj) {
                const usageStrs = [];
                if (Array.isArray(usageObj)) {
                    usageObj.forEach((u) => usageStrs.push(u));
                } else {
                    usageStrs.push(usageObj.toString());
                }

                usageStrs.forEach((u) =>
                    (this.usageList += `\n- ${this.settings.bot_cmd}${u}`));
            }
        });
    }

    /**
     * TayneBot's init.
     * @return {Promise} pronise to login client
     */
    init() {
        // load commands
        console.log('Loading commands...');
        this.loadCommands(this.settings.commands);

        // setup events
        console.log('Setting up event bindings...');
        this.client
        .on('ready', this.onReady())
        // .on('serverNewMember', this.serverNewMember())
        .on('message', this.onMessage())
        .on('error', this.onError())
        .on('voiceStateUpdate', this.onVoiceStateUpdate())
        .on('disconnect', this.onDisconnected());

        console.log('Connecting...');
        // return the promise from "login()"
        return this.client.login(this.token);
    }

    /**
     * TayneBot's deinit.
     * @return {Promise} promise to destroy client
     */
    deinit() {
        if (this.isReady) {
            // disconnect gracefully
            this.isReady = false;
            // return the promise from "destroy()"
            return this.client.destroy();
        }
    }

    /**
     * returns true if member is Admin or Mod.
     * @param {String} member - Member to check
     * @return {boolean} if member is Admin/Mod
     */
    isAdminOrMod(member) {
        const immuneRoles = new Set(this.settings.voting.immuneRoles);
        const userRoles = new Set(member.roles.array().map((r) => r.name));
        const setIntersection =
            [...userRoles].filter((r) => immuneRoles.has(r));
        return setIntersection.length > 0;
    }
}

module.exports = TayneBot;
