#!/usr/bin/env node

const TayneBot = require('./taynebot.js');
const Tokens = require('./tokens.json');
const Env = process.env.ENVIRONMENT;

/**
 * Starts the bot
 */
function start() {
    // check for the discord token
    let jsonToken = false;
    if (Tokens) {
        jsonToken = Env === 'prod' ? Tokens.discord.prod : Tokens.discord.dev;
    }
    const token = jsonToken || process.env.DISCORD_TOKEN;
    if (!token) {
        throw Error('Discord token not set');
    }

    let tayne = new TayneBot(token);
    tayne.init();

    process.on('exit', () => {
        console.log('on exit');
        tayne.deinit();
    });

    process.on('SIGINT', () => {
        console.log('on SIGINT');
        tayne.deinit();
    });

    process.on('SIGTERM', () => {
        console.log('on SIGTERM');
        tayne.deinit();
    });

    process.on('uncaughtException', (err) => {
        console.log('Caught exception: ' + err);
        tayne.deinit();
    });
}

start();
