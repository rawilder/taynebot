const fs = require('fs');

const memeDir = './sounds/meme/';
const memeFiles = [];
fs.readdir(memeDir, (err, files) => {
    files.forEach((file) => {
        memeFiles.push(memeDir + file);
    });
    // console.log(memeFiles);
});

let usage = 'meme - Just memes, man.';

module.exports = {
    type: 'audio',
    usage: usage,
    run: (bot, voiceChannel, filename='') => {
        if (voiceChannel != null) {
            voiceChannel.join()
            .then((connection) => {
                let memeToPlay =
                    memeFiles[Math.floor(Math.random()*memeFiles.length)];
                if (filename != '') {
                    memeToPlay = memeDir + filename;
                }
                console.log('playing ' + memeToPlay);
                bot.playingAudio = true;
                const dispatcher = connection.playFile(memeToPlay);
                dispatcher.on('debug', (debug) => {
                    console.log('debug: ', debug);
                });
                dispatcher.on('error', (err) => {
                    console.log('error: ', err);
                });
                dispatcher.on('start', (start) => {
                    // console.log('start');
                });
                connection.on('error', (err) => {
                    console.log('error : ', err);
                });
                connection.on('disconnected', (err) => {
                    console.log('disconnected : ', err);
                });
                dispatcher.on('end', (end) => {
                    // console.log('end');
                    voiceChannel.leave();
                    bot.playingAudio = false;
                });
            })
            .catch((err) => console.log(err));
        }
    },
    showUsage: (bot, message) => {
        if (typeof usage !== 'string') {
            usage = usage.join('\n');
        }
        message.channel.send('```\n' + usage + '\n```');
    },
};
