const fs = require('fs');

const paulaDir = './sounds/paula/';
const paulaFiles = [];
fs.readdir(paulaDir, (err, files) => {
    files.forEach((file) => {
        paulaFiles.push(paulaDir + file);
    });
    // console.log(paulaFiles);
});

let usage = 'paula - Hey y\'all.';

module.exports = {
    type: 'audio',
    usage: usage,
    run: (bot, voiceChannel, heyYall=false) => {
        if (voiceChannel != null) {
            voiceChannel.join()
            .then((connection) => {
                let paulaToPlay =
                    paulaFiles[Math.floor(Math.random()*paulaFiles.length)];
                if (heyYall) {
                    paulaToPlay = './sounds/paula/hey_yall.mp3';
                }
                console.log('playing ' + paulaToPlay);
                bot.playingAudio = true;
                const dispatcher = connection.playFile(paulaToPlay);
                dispatcher.on('debug', (debug) => {
                    console.log('debug: ', debug);
                });
                dispatcher.on('error', (err) => {
                    console.log('error: ', err);
                });
                dispatcher.on('start', (start) => {
                    // console.log('start');
                });
                connection.on('error', (err) => {
                    console.log('error : ', err);
                });
                connection.on('disconnected', (err) => {
                    console.log('disconnected : ', err);
                });
                dispatcher.on('end', (end) => {
                    // console.log('end');
                    voiceChannel.leave();
                    bot.playingAudio = false;
                });
            })
            .catch((err) => console.log(err));
        }
    },
    showUsage: (bot, message) => {
        if (typeof usage !== 'string') {
            usage = usage.join('\n');
        }
        message.channel.send('```\n' + usage + '\n```');
    },
};
