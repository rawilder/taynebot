const discord = require('discord.js');

let usage = 'server - prints info about the server';

module.exports = {
    type: 'text',
    usage: usage,
    run: (bot, message) => {
        const embed = new discord.RichEmbed();
        embed.setTitle('Server Owner')
                .setColor('#ff7260')
                .setAuthor(message.guild.name, message.guild.iconURL)
                .setDescription(message.guild.owner.user.username)
                .addField('Members', message.guild.members.size, true)
                .addField('Created', message.guild.createdAt.toString(), true)
                .addField('Emojis',
                message.guild.emojis.size > 0 ? message.guild.emojis
                    .map((d) => d.toString()).join(' ') : 'None');
        message.channel.send('', {embed: embed});
    },
    showUsage: (bot, message) => {
        if (typeof usage !== 'string') {
            usage = usage.join('\n');
        }
        message.channel.send('```\n' + usage + '\n```');
    },
};
