const fs = require('fs');

const tayneDir = './sounds/4d3d3d3/';
const tayneFiles = [];
fs.readdir(tayneDir, (err, files) => {
    files.forEach((file) => {
        tayneFiles.push(tayneDir + file);
    });
    // console.log(tayneFiles);
});

let usage = 'tayne - I can\'t wait to entertain you.';

module.exports = {
    type: 'audio',
    usage: usage,
    run: (bot, voiceChannel) => {
        if (voiceChannel != null) {
            voiceChannel.join()
            .then((connection) => {
                let tayneToPlay =
                    tayneFiles[Math.floor(Math.random()*tayneFiles.length)];
                console.log('playing ' + tayneToPlay);
                bot.playingAudio = true;
                const dispatcher = connection.playFile(tayneToPlay);
                dispatcher.on('debug', (debug) => {
                    console.log('debug: ', debug);
                });
                dispatcher.on('error', (err) => {
                    console.log('error: ', err);
                });
                dispatcher.on('start', (start) => {
                    // console.log('start');
                });
                connection.on('error', (err) => {
                    console.log('error : ', err);
                });
                connection.on('disconnected', (err) => {
                    console.log('disconnected : ', err);
                });
                dispatcher.on('end', (end) => {
                    // console.log('end');
                    voiceChannel.leave();
                    bot.playingAudio = false;
                });
            })
            .catch((err) => console.log(err));
        }
    },
    showUsage: (bot, message) => {
        if (typeof usage !== 'string') {
            usage = usage.join('\n');
        }
        message.channel.send('```\n' + usage + '\n```');
    },
};
