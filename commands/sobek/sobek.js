const fs = require('fs');

const sobekDir = './sounds/sobek/';
const sobekFiles = [];
fs.readdir(sobekDir, (err, files) => {
    files.forEach((file) => {
        sobekFiles.push(sobekDir + file);
    });
    // console.log(sobekFiles);
});

let usage = 'sobek - Me booty is the best booty';

module.exports = {
    type: 'audio',
    usage: usage,
    run: (bot, voiceChannel) => {
        if (voiceChannel != null) {
            voiceChannel.join()
            .then((connection) => {
                let sobekToPlay =
                    sobekFiles[Math.floor(Math.random()*sobekFiles.length)];
                console.log('playing ' + sobekToPlay);
                bot.playingAudio = true;
                const dispatcher = connection.playFile(sobekToPlay);
                dispatcher.on('debug', (debug) => {
                    console.log('debug: ', debug);
                });
                dispatcher.on('error', (err) => {
                    console.log('error: ', err);
                });
                dispatcher.on('start', (start) => {
                    // console.log('start');
                });
                connection.on('error', (err) => {
                    console.log('error : ', err);
                });
                connection.on('disconnected', (err) => {
                    console.log('disconnected : ', err);
                });
                dispatcher.on('end', (end) => {
                    // console.log('end');
                    voiceChannel.leave();
                    bot.playingAudio = false;
                });
            })
            .catch((err) => console.log(err));
        }
    },
    showUsage: (bot, message) => {
        if (typeof usage !== 'string') {
            usage = usage.join('\n');
        }
        message.channel.send('```\n' + usage + '\n```');
    },
};
