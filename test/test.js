const test = require('tape-catch');
const TayneBot = require('../taynebot.js');

const token =
    process.env.DISCORD_TOKEN || require('../tokens.json').discord.test;

test('connect & disconnect', (t) => {
    t.timeoutAfter(15000);
    t.ok(token, 'discord token should be set');

    const bot = new TayneBot(token);
    t.false(bot.isReady, 'bot should not be ready');
    bot.init();
    // wait for it to be ready
    const si = setInterval(() => {
        if (bot.isReady) {
            bot.deinit().then(() => {
                clearInterval(si);
                t.end();
            });
        }
    }, 5000);
});
