#/bin/bash

sudo sed -i 's/^mesg n .*$/tty -s \&\& mesg n || true/g' /root/.profile
sudo su -
apt-get update && apt-get -y install ffmpeg g++ make python
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
echo "setting up taynebot user and directories"
id -u taynebot &>/dev/null || useradd -m -s /bin/bash taynebot
mkdir -p /home/taynebot/taynebot
cp -p /home/ubuntu/deployment.tar.gz /home/taynebot/
cd /home/taynebot
chown -R taynebot:taynebot .
su - taynebot
cd taynebot
echo "stopping taynebot"
pkill -F ./taynebot.pid
echo "deploying new code"
rm -rf *
tar xf ../deployment.tar.gz
chown -R taynebot:taynebot .
npm install
echo "starting taynebot"
export ENVIRONMENT="prod"
nohup ./start.js > taynebot.log 2>&1&
echo $! > taynebot.pid

# exit
# cp /home/taynebot/taynebot/taynebot.service /etc/systemd/system/
# systemctl daemon-reload
# systemctl enable taynebot
# echo "restarting taynebot"
# systemctl restart taynebot
